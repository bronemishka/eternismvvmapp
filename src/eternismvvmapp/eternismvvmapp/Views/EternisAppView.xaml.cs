﻿using System;
using System.IO.Ports;
using eternismvvmapp.ViewModels;

namespace eternismvvmapp.Views
{
    /// <summary>
    /// Interaction logic for EternisAppView.xaml
    /// </summary>
    public partial class EternisAppView
    {
        public EternisAppView()
        {
            InitializeComponent();
            DataContext = new EternisAppViewModel();
        }

        private void UpdateComboBox(object? sender, EventArgs e)
        {
            ((EternisAppViewModel)DataContext).ComPortList = SerialPort.GetPortNames(); // Not a best way to update combobox imo
        }
    }
}
