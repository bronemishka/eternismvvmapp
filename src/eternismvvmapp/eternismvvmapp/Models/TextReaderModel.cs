﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace eternismvvmapp.Models
{
    public class TextReaderModel
    {
        public static int AddressSize = 100;

        public int[] AddressList = new int[AddressSize];

        public void ParseTextFromFile(string path)
        {
            FillArray(File.ReadAllText(path).Split(Environment.NewLine).ToList().Select(int.Parse).ToArray());
        }

        private void FillArray(IReadOnlyCollection<int> nums)
        {
            if (AddressSize < nums.Count) throw new IndexOutOfRangeException();
            AddressList = new int[AddressSize];
            var spaceLen = ((double)AddressList.Length - nums.Count) / nums.Count;
            var pos = 0.0;
            foreach (var item in nums)
            {
                AddressList[(int)Math.Round(pos)] = item;
                pos += spaceLen + 1;
            }
        }

        public string GetTextPreview()
        {
            var result = string.Empty;
            for (var i = 0; i < AddressList.Length; i++)
            {
                if (AddressList[i] == 0) continue;
                result += $"Адрес {i+1} – серийный номер {AddressList[i]}\n";
            }

            return result;
        }
    }
}