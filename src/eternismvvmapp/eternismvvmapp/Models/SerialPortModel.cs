﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;

namespace eternismvvmapp.Models
{
    public class SerialPortModel
    {
        public bool CanChange;
        private SerialPort _comPort;
        public SerialPortModel(string portName)
        {
            CanChange = true;
            _comPort = new SerialPort(portName)
            {
                Handshake = Handshake.None,
                Encoding = Encoding.Default,
                ReadTimeout = 500,
                WriteTimeout = 500
            };
        }
        public void OpenPort(string portName)
        {
            if (portName != _comPort.PortName)
            {
                _comPort = new SerialPort(portName)
                {
                    Handshake = Handshake.None,
                    Encoding = Encoding.Default,
                    ReadTimeout = 500,
                    WriteTimeout = 500
                };
            }
            _comPort.Open();
            CanChange = false;
        }
        public void PassDataToComPort(List<string>? data)
        {
            if (CanChange) return;
            if (!_comPort.IsOpen) return;
            data?.ForEach(_comPort.WriteLine);
        }

        public void ClosePort()
        {
            _comPort.Close();
            CanChange = true;
        }
    }
}
