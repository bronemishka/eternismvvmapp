﻿using System;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using eternismvvmapp.Models;
using Microsoft.Win32;

namespace eternismvvmapp.ViewModels
{
    public class EternisAppViewModel : ObservableObject
    {
        public EternisAppViewModel()
        {
            SelectFileClick = new RelayCommand(SelectFile);
            OpenComPortConnection = new RelayCommand(OpenConnection);
            CloseComPortConnection = new RelayCommand(CloseConnection);
            PassDataToPort = new RelayCommand(SendDataToPort);
            ComPortList = SerialPort.GetPortNames();
            ComPortSelected = ComPortList?.FirstOrDefault();
            _portModel = new SerialPortModel(ComPortSelected);
        }

        private readonly TextReaderModel _readerModel = new();
        private readonly SerialPortModel _portModel;
        public IRelayCommand SelectFileClick { get; }
        public IRelayCommand PassDataToPort { get; }
        public IRelayCommand OpenComPortConnection { get; }
        public IRelayCommand CloseComPortConnection { get; }
        private string _outputText = "...";

        public string OutputText
        {
            get => _outputText;
            set => SetProperty(ref _outputText, value);
        }

        private string _filePathText = "File not selected";

        public string FilePathText
        {
            get => _filePathText;
            set => SetProperty(ref _filePathText, value);
        }

        private bool _portConnectionClosed = true;

        public bool PortConnectionClosed
        {
            get => _portConnectionClosed;
            set => SetProperty(ref _portConnectionClosed, value);
        }
        private bool _portConnectionOpened;

        public bool PortConnectionOpened
        {
            get => _portConnectionOpened;
            set => SetProperty(ref _portConnectionOpened, value);
        }

        private string[] _comPortList;

        public string[] ComPortList
        {
            get => _comPortList;
            set => SetProperty(ref _comPortList, value);
        }

        private string _comPortSelected;

        public string ComPortSelected
        {
            get => _comPortSelected;
            set => SetProperty(ref _comPortSelected, value);
        }

        public int AddressSize
        {
            get => TextReaderModel.AddressSize;
            set
            {
                if (value < 0) value = TextReaderModel.AddressSize;
                try
                {
                    SetProperty(ref TextReaderModel.AddressSize, value);
                }
                catch
                {
                    // ignored
                }
            }
        }

        private void ToggleConnection()
        {
            PortConnectionClosed = !PortConnectionClosed;
            PortConnectionOpened = !PortConnectionOpened;
        }

        private void OpenConnection()
        {
            _portModel.OpenPort(ComPortSelected);
            ToggleConnection();
        }

        private void SendDataToPort()
        {
            _portModel.PassDataToComPort(_readerModel.GetTextPreview().Split(Environment.NewLine).ToList());
        }
        private void CloseConnection()
        {
            _portModel.ClosePort();
            ToggleConnection();
        }

        private void SelectFile()
        {
            var openFileDialog = new OpenFileDialog
            {
                InitialDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*",
                FilterIndex = 1,
                Multiselect = false
            };
            if (openFileDialog.ShowDialog() != true) return;
            FilePathText = openFileDialog.FileName;
            ProcessFile(FilePathText);
        }

        private void ProcessFile(string path)
        {
            _readerModel.ParseTextFromFile(path);
            OutputText = _readerModel.GetTextPreview();
        }
    }
}
